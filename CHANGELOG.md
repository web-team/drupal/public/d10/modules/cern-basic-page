# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.2] - 27/10/2023

- Updated `allowed_formats` to version `^3.0`.
- Updated `ctools` to version `^4.0`.
- Updated `pathauto` to version `^1.12`.
- Updated `smart_trim` to version `^2.1`.
- Updated `scheduler` to version `^2.0`.
- Updated `color_field` to version `^3.0`.

## [3.0.1] - 07/02/2023
- Bump dependencies to ensure continued compatibility.

## [3.0.0] - 14/10/2022
- Update module and ensure PHP 8.1 compatibility.

## [2.0.3] - 29/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.0.2] - 13/10/2021

- Add D9 readiness

## [2.0.1] - 28/05/2021

- Add composer support
- Add CHANGELOG
